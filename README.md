# Medusa Head for SparkFun ESP32 Thing

![Medusa Head for SparkFun ESP32 Thing](http://learn.edwinrobotics.com/wp-content/uploads/Medusa-Head-for-ESP32.jpg "Medusa Head for SparkFun ESP32 Thing")

[Medusa Head for SparkFun ESP32 Thing](https://shop.edwinrobotics.com/medusa/1180-medusa-head-for-sparkfun-esp32-thing.html)

[Medusa Head for SparkFun ESP32 Thing (including ESP32)](https://shop.edwinrobotics.com/medusa/1181-medusa-head-for-sparkfun-esp32-thing-esp32-included.html)

The Medusa Head for SparkFun ESP32 Thing breaks out  15 of  the 26 pins I/O. In addition to this, we have also provided four i2c ports providing ample ports for connecting i2c based interfaces and access to the Serial port.


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.